all: help


VIRTUALENV=virtualenv

help: help-env-clean
help-env-clean:
	@echo "env-clean"
env-clean:
	rm -rf env

help: help-env
help-env:
	@echo "env"
env:
	$(VIRTUALENV) --no-site-packages env
	env/bin/pip install -r reqs.txt

help: help-runserver
help-runserver:
	@echo "runserver"
runserver: env
	./main.py

log:
	mkdir -vp log

ifeq (yes,$(DAEMONIZE))
OPT_UWSGI_DAEMONIZE=--daemonize /dev/null
endif

help: help-uwsgi
help-uwsgi:
	@echo "uwsgi [ DAEMONIZE=yes ]"
uwsgi: env
	env/bin/uwsgi --chdir . -s smime_ui.socket --chmod=777 --module main --callable app --master --processes 2 $(OPT_UWSGI_DAEMONIZE)

help: help-uwsgi_http
help-uwsgi_http:
	@echo "uwsgi_http [ HOST=0.0.0.0 ] [ PORT=6782 ] [ DAEMONIZE=yes ]"
uwsgi_http: env
	@echo URL - http://localhost:$(if $(PORT),$(PORT),6782)/ensmime
	env/bin/uwsgi --chdir . --http $(if $(HOST),$(HOST),0.0.0.0):$(if $(PORT),$(PORT),6782) --module main --callable app --master --processes 2 $(OPT_UWSGI_DAEMONIZE)

help: help-god
help-god:
	@echo "god"
god:
	god load smime_ui.god

dot-ssh:
	[ -d $$HOME/.ssh ] || mkdir $$HOME/.ssh

help: help-mkkey
help-mkkey:
	@echo "mkkey"
mkkey: dot-ssh
	[ -e $$HOME/.ssh/id_rsa ] || ssh-keygen -t rsa -b 2048

help: help-mkkey-openssl
help-mkkey-openssl:
	@echo "mkkey-openssl"
mkkey-openssl: dot-ssh
	[ -e $$HOME/.ssh/id_rsa ] || openssl genrsa -aes256 -out $$HOME/.ssh/id_rsa 2048

help: help-mkcert
help-mkcert:
	@echo "mkcert"
mkcert: mkkey
	[ -e $$HOME/.ssh/id_rsa.cert.pem ] || openssl req -new -x509 -key $$HOME/.ssh/id_rsa -out $$HOME/.ssh/id_rsa.cert.pem

help: help-must-choose
help-must-choose:
	@false

PREFIX=/opt/ende-ui
DEPLOY_SRC=.
PUBLIC_BIN=/usr/local/bin

deploy:
	@echo "### deploying requires sudo - you may be prompted for your user password.. ###"
	[ -e $(PREFIX) ] || sudo $(VIRTUALENV) --no-site-packages $(PREFIX)
	sudo $(PREFIX)/bin/pip install -U $(DEPLOY_SRC)
	[ -e $(PUBLIC_BIN)/ende-ui ] || sudo ln -vs $(PREFIX)/bin/ende-ui $(PUBLIC_BIN)/ende-ui
