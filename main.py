#!/usr/bin/env env/bin/python

"""
Copyright (C) 2015 DK

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import sys
import os
from flask import Flask, request, render_template, render_template_string, Response
import subprocess
import pipes
import base64
import argparse
import email, email.utils
import version
import urllib

app = Flask(__name__)

TEMPLATE_VERSION = """
<title>ende-ui version {{Version}}</title>
ende-ui version {{Version}}
"""

@app.route("/version", methods=['GET'])
def view_version():
	return render_template_string(TEMPLATE_VERSION, Version=version.VERSION)

TEMPLATE_ENCRYPT = """
<title>ende-ui - encrypt</title>
<h1>encrypt / <a href="decrypt">decrypt</a></h1>
<p><form action="encrypt" method=POST>
<p>Passphrase: <input type=password name=Passphrase size=64 value="">
<p>Plain Text:<br>
<textarea name=PlainText cols=120 rows=25></textarea>
<p><input type=submit value="Encrypt">
</form>
{% if Error %}
<hr>
<pre style="color: red;">{{Error|escape}}</pre>
{% endif %}
{% if EncText %}
<hr>
Encrypted text:<br>
<textarea cols="{{2+EncCols|default(80)}}" rows="{{2+EncLines|default(0)}}">{{EncText|escape}}</textarea>
{% endif %}
"""

@app.route("/encrypt", methods=['POST', 'GET'])
def encrypt():
	Passphrase=request.form.get('Passphrase', '')
	PlainText=request.form.get('PlainText', '')
	EncText = ''
	EncCols = 0
	Error = ''
	if request.method == 'POST' and PlainText:
		if not Passphrase:
			Error = 'Passphrase must be supplied'
		elif '\n' in Passphrase:
			Error = 'Passphrase must not contain newlines'
		else:
			PlainText=PlainText.replace('\r\n','\n').replace('\r','\n')
			Args = ('openssl', 'enc', '-a', '-aes256', '-pass', 'stdin')
			print 'Args:', repr(Args)
			Process = subprocess.Popen(Args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			Input = '%s\n%s' % (Passphrase.encode('utf8'), PlainText.encode('utf8'))
			(EncText, Error) = Process.communicate(Input)
			EncCols = max(map(len, ['x'*64]+list(EncText.splitlines())))
	return render_template_string(TEMPLATE_ENCRYPT, EncText=EncText.strip(), EncLines=len(EncText.splitlines()), EncCols=EncCols, Error=Error)

TEMPLATE_DECRYPT = """
<title>ende-ui - decrypt</title>
<h1><a href="encrypt">encrypt</a> / decrypt</h1>
<p><form action="decrypt" method=POST>
<p>Passphrase: <input type=password name=Passphrase size=64 value="">
<p>Encrypted Text:<br>
<textarea name=EncText cols=120 rows=25>{{EncText|escape}}</textarea>
<p><input type=submit value="Decrypt">
</form>
{% if Error %}
<hr>
<pre style="color: red;">{{Error|escape}}</pre>
{% endif %}
{% if PlainText %}
<hr>
<script>
function textHidden(){ var Text = document.getElementById("textOutput"); Text.style.color="#94B5FE"; Text.style.background="#94B5FE"; Text.style.display=""; }
function textBleak(){ var Text = document.getElementById("textOutput"); Text.style.color="#A7A7FF"; Text.style.background="#A7A7FF"; Text.style.display=""; }
function textPlain(){ var Text = document.getElementById("textOutput"); Text.style.color=""; Text.style.background=""; Text.style.display=""; }
</script>
<p>Show decrypted text as:
 | <a onclick="javascript:textHidden();">[hidden]</a>
 | <a onclick="javascript:textBleak();">[bleak]</a>
 | <a onclick="javascript:textPlain();">[plain]</a>
<br>
Decrypted text:<br>
<textarea id="textOutput" cols="{{2+(PlainCols|default(80))}}" rows="{{2+PlainLines|default(0)}}" style="font-family: menlo; display: none;">{{PlainText|escape}}</textarea>
<script>
textHidden();
</script>
{% endif %}
"""

@app.route("/decrypt", methods=['POST', 'GET'])
def decrypt():
	Passphrase=request.form.get('Passphrase', '')
	EncText=request.form.get('EncText', '')
	PlainTextUtf8 = ''
	Error = ''
	PlainCols = 0
	if request.method == 'POST' and EncText:
		if not Passphrase:
			Error = 'Passphrase must be supplied'
		elif '\n' in Passphrase:
			Error = 'Passphrase must not contain newlines'
		else:
			EncText=EncText.replace('\r\n','\n').replace('\r','\n')
			EncTextAligned=base64.encodestring(base64.decodestring(EncText))
			Args = ('openssl', 'enc', '-d', '-a', '-aes256', '-pass', 'stdin')
			print 'Args:', repr(Args)
			Process = subprocess.Popen(Args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			(PlainText, Error) = Process.communicate('%s\n%s\n' % (Passphrase, EncTextAligned.encode('utf8')))
			if Process.returncode != 0:
				PlainText = ''
			PlainTextUtf8 = PlainText.decode('utf8', 'replace')
			PlainCols = max(map(len, ['x'*80]+list(PlainText.splitlines())))
	return render_template_string(TEMPLATE_DECRYPT, EncText=EncText.strip(), PlainText=PlainTextUtf8, PlainLines=len(PlainTextUtf8.splitlines()), PlainCols=PlainCols, Error=Error)

TEMPLATE_ENSMIME = """
<title>ende-ui - ensmime</title>
<h1>ensmime / <a href="desmime">desmime</a></h1>
<p><form action="ensmime" method=POST>
<p>Cert(s): <input type=text name=Cert size=80 value="{{Cert|default('')}}">
<p>Subject: <input type=text name=Subject size=64 value="{{Subject|default('')}}">
<p>Plain Text:<br>
<textarea name=PlainText cols=120 rows=25></textarea>
<p><input type=submit value="Encrypt">
</form>
{% if Error %}
<hr>
<pre style="color: red;">{{Error|escape}}</pre>
{% endif %}
{% if EncText %}
<hr>
<textarea cols=120 rows="{{EncLines}}">{{EncText|escape}}</textarea>
{% endif %}
"""

def home():
	if sys.platform=='win32':
		return os.getenv('USERPROFILE')
	return os.getenv('HOME')

def user():
	if sys.platform=='win32':
		return os.getenv('USERNAME')
	return os.getenv('USER')

@app.route("/ensmime", methods=['POST', 'GET'])
def ensmime():
	Cert=request.form.get('Cert', request.args.get('Cert','')).replace(',',' ').strip()
	Subject=request.form.get('Subject', request.args.get('Subject',''))
	PlainText=request.form.get('PlainText', '')
	EncText = ''
	Error = ''
	if request.method == 'POST' and PlainText:
		PlainText=PlainText.replace('\r\n','\n').replace('\r','\n')
		HaveCert = False
		CertMap = {
			'': os.path.join(home(), '.ssh/id_rsa.cert.pem')
		}
		CertEntryList = []
		CertHome = os.path.join(home(), '.ende-ui/cert')
		if os.path.isdir(CertHome):
			CertEntryList = list(os.listdir(CertHome))
		for CertEntry in CertEntryList:
			if CertEntry[-len('.cert.pem'):] == '.cert.pem':
				CertMap[CertEntry[:-len('.cert.pem')]] = os.path.join(CertHome,CertEntry)
		CertFileNameList = []
		HaveCertError = False
		print 'Cert:', repr(Cert)
		for CertWord in Cert.split() or ['']:
			CertFileName = CertMap.get(CertWord)
			if not CertFileName:
				Error = 'Cert not available: %s' % CertWord
				HaveCertError = True
				break
			elif not os.path.exists(CertFileName):
				HaveCertError = True
				if Cert:
					Error = 'Cert doesn\'t exist: %s' % CertWord
				else:
					Error = 'Default cert doesn\'t exist'
				break
			else:
				CertFileNameList.append(CertFileName)
		print 'CertFileNameList: %s' % repr(CertFileNameList)
		print 'HaveCertError: %s' % HaveCertError
		if CertFileNameList and not HaveCertError:
			if Cert:
				ToHeader = ', '.join(Cert.split())
			else:
				ToHeader = user()
			Args = ('openssl', 'smime', '-encrypt', '-aes256', '-to', ToHeader, '-subject', Subject) + tuple(CertFileNameList)
			print 'Args:', repr(Args)
			Process = subprocess.Popen(Args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			(EncText, Error) = Process.communicate(PlainText.encode('utf8'))
			if not Error:
				# tweak the message - add Date header - hacky
				EncText = EncText.replace('MIME-Version: 1.0', 'Date: %s\nMIME-Version: 1.0' % email.utils.formatdate())
	return render_template_string(TEMPLATE_ENSMIME, Cert=Cert, Subject=Subject, EncText=EncText.strip(), EncLines=len(EncText.splitlines()), Error=Error)

TEMPLATE_DESMIME = """
<title>ende-ui - desmime</title>
<h1><a href="ensmime">ensmime</a> / desmime</h1>
<p><form action="desmime" method=POST>
<p>Key Name: <input type=text name=Key value="{{Key|default('')}}"> (leave empty to use default id_rsa key)
<p>Key Password: <input type=password name=Password size=64 value=""> (for the private key)
<p>CipherText:<br>
<textarea name=CipherText cols=120 rows=25>{{CipherText|default('')|escape}}</textarea>
<p><input type=submit value="Decrypt">
</form>
{% if SubjectList %}
<hr>
{% for Subject in SubjectList %}
<p>Subject: <b>{{Subject|escape}}</b>
{% endfor %}
{% endif %}
{% if Error %}
<hr>
<pre style="color: red;">{{Error|escape}}</pre>
{% endif %}
{% if PlainText %}
<hr>
<script>
function textHidden(){ var Text = document.getElementById("textOutput"); Text.style.color="#94B5FE"; Text.style.background="#94B5FE"; Text.style.display=""; }
function textBleak(){ var Text = document.getElementById("textOutput"); Text.style.color="#A7A7FF"; Text.style.background="#A7A7FF"; Text.style.display=""; }
function textPlain(){ var Text = document.getElementById("textOutput"); Text.style.color=""; Text.style.background=""; Text.style.display=""; }
</script>
<p>Show decrypted text as:
 | <a onclick="javascript:textHidden();">[hidden]</a>
 | <a onclick="javascript:textBleak();">[bleak]</a>
 | <a onclick="javascript:textPlain();">[plain]</a>
<br>
<p>Decrypted text:<br>
<textarea id=textOutput cols=120 rows="{{PlainTextLines|default(4)}}" style="font-family: menlo; display: none;">{{PlainText|escape}}</textarea>
<script>
textHidden();
</script>
{% endif %}
"""

@app.route("/desmime", methods=['POST', 'GET'])
def desmime():
	Key=request.form.get('Key', request.args.get('Key',''))
	Password=request.form.get('Password', '')
	CipherText=request.form.get('CipherText', '')
	SubjectList=[]
	PlainText = ''
	Error = ''
	if request.method == 'POST' and CipherText:
		try:
			Message = email.message_from_string(CipherText)
			SubjectList = Message.get_all('Subject')
		except:
			pass
		KeyMap = {
			'': os.path.join(home(), '.ssh/id_rsa')
		}
		KeyEntryList = []
		KeyHome = os.path.join(home(), '.ende-ui/key')
		if os.path.isdir(KeyHome):
			KeyEntryList = list(os.listdir(KeyHome))
		for KeyEntry in KeyEntryList:
			if KeyEntry[-len('.key.pem'):] == '.key.pem':
				KeyMap[KeyEntry[:-len('.key.pem')]] = os.path.join(KeyHome,KeyEntry)
		HaveKey = False
		KeyFileName = KeyMap.get(Key)
		if not KeyFileName:
			Error = 'Key not available: %s' % Key
		elif not os.path.exists(KeyFileName):
			if Key:
				Error = 'Key doesn\'t exist: %s' % Key
			else:
				Error = 'Default key doesn\'t exist'
		else:
			HaveKey = True
		print 'KeyFileName: %s' % repr(KeyFileName)
		print 'HaveKey: %s' % HaveKey
		if HaveKey:
			Args = ('openssl', 'smime', '-decrypt', '-passin', 'stdin', '-inkey', KeyFileName)
			print 'Args:', repr(Args)
			Process = subprocess.Popen(Args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			(PlainText, Error) = Process.communicate('%s\n%s' % (Password, CipherText))
	return render_template_string(TEMPLATE_DESMIME, Key=Key, SubjectList=SubjectList, CipherText=CipherText.strip(), PlainText=PlainText, PlainTextLines=len(PlainText.splitlines()), Error=Error)

TEMPLATE_ENGPG = """
<title>ende-ui - engpg</title>
<h1>engpg / <a href="degpg">degpg</a></h1>
<p><form action="engpg" method=POST>
<p>Recipient(s): <input type=text name=Cert size=80 value="{{Cert|default('')}}">
<p>Subject: <input type=text name=Subject size=64 value="{{Subject|default('')}}">
<p>Plain Text:<br>
<textarea name=PlainText cols=120 rows=25></textarea>
<p><input type=submit value="Encrypt">
</form>
{% if Error %}
<hr>
<pre style="color: red;">{{Error|escape}}</pre>
{% endif %}
{% if EncText %}
<hr>
<textarea cols=120 rows="{{EncLines}}">{{EncText|escape}}</textarea>
{% endif %}
"""

@app.route("/engpg", methods=['POST', 'GET'])
def engpg():
	Cert=request.form.get('Cert', request.args.get('Cert','')).replace(',',' ').strip()
	Subject=request.form.get('Subject', request.args.get('Subject',''))
	PlainText=request.form.get('PlainText', '')
	EncText = ''
	Error = ''
	if request.method == 'POST' and PlainText:
		PlainText=PlainText.replace('\r\n','\n').replace('\r','\n')
		ArgsBase = ('gpg', '-a', '--encrypt', '--batch', '--trust-model', 'always', '--no-auto-key-locate',)
		ArgsComments = (
			'--comment', 'Subject: %s' % Subject,
			'--comment', 'To: %s' % ', '.join(Cert.split()),
			'--comment', 'Date: %s' % email.utils.formatdate(),
		)
		ArgsRcpt = tuple(sum([ [ '-r', RcptItem ] for RcptItem in Cert.split() ], []))
		Args = ArgsBase + ArgsComments + ArgsRcpt
		print 'Args:', repr(Args)
		Process = subprocess.Popen(Args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		(EncText, Error) = Process.communicate(PlainText.encode('utf8'))
	return render_template_string(TEMPLATE_ENGPG, Cert=Cert, Subject=Subject, EncText=EncText.strip(), EncLines=len(EncText.splitlines()), Error=Error)

TEMPLATE_DEGPG = """
<title>ende-ui - degpg</title>
<h1><a href="engpg">engpg</a> / degpg</h1>
<p><form action="degpg" method=POST>
<p>Key Passphrase: <input type=password name=Password size=64 value=""> (for the private key)
<p>CipherText:<br>
<textarea name=CipherText cols=120 rows=25>{{CipherText|default('')|escape}}</textarea>
<p><input type=submit value="Decrypt">
</form>
{% if SubjectList %}
<hr>
{% for Subject in SubjectList %}
<p>Subject: <b>{{Subject|escape}}</b>
{% endfor %}
{% endif %}
{% if Error %}
<hr>
<pre style="color: red;">{{Error|escape}}</pre>
{% endif %}
{% if PlainText %}
<hr>
<script>
function textHidden(){ var Text = document.getElementById("textOutput"); Text.style.color="#94B5FE"; Text.style.background="#94B5FE"; Text.style.display=""; }
function textBleak(){ var Text = document.getElementById("textOutput"); Text.style.color="#A7A7FF"; Text.style.background="#A7A7FF"; Text.style.display=""; }
function textPlain(){ var Text = document.getElementById("textOutput"); Text.style.color=""; Text.style.background=""; Text.style.display=""; }
</script>
<p>Show decrypted text as:
 | <a onclick="javascript:textHidden();">[hidden]</a>
 | <a onclick="javascript:textBleak();">[bleak]</a>
 | <a onclick="javascript:textPlain();">[plain]</a>
<br>
<p>Decrypted text:<br>
<textarea id=textOutput cols=120 rows="{{PlainTextLines|default(4)}}" style="font-family: menlo; display: none;">{{PlainText|escape}}</textarea>
<script>
textHidden();
</script>
{% endif %}
"""

@app.route("/degpg", methods=['POST', 'GET'])
def degpg():
	Key=request.form.get('Key', request.args.get('Key',''))
	Password=request.form.get('Password', '')
	CipherText=request.form.get('CipherText', '')
	SubjectList=[]
	PlainText = ''
	Error = ''
	if request.method == 'POST' and CipherText:
		Args = ('gpg', '-a', '--decrypt', '--passphrase-fd', '0', '--batch', '-q',)
		print 'Args:', repr(Args)
		Process = subprocess.Popen(Args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		(PlainText, Error) = Process.communicate('%s\n%s' % (Password, CipherText))
	return render_template_string(TEMPLATE_DEGPG, Key=Key, SubjectList=SubjectList, CipherText=CipherText.strip(), PlainText=PlainText, PlainTextLines=len(PlainText.splitlines()), Error=Error)

def main():
	parser = argparse.ArgumentParser(description="ende-ui", version=version.VERSION)
	parser.add_argument('--host', '-H', dest='host', type=str, help='host to listen on', default='127.0.0.1')
	parser.add_argument('--port', '-p', dest='port', type=int, help='port to listen on', default=6782)
	parser.add_argument('--debug', '-d', dest='debug', action='store_true', help='enable flask debug option', default=False)
	parser.add_argument('--mkkey', action='store_true', help='generate key - openssl genrsa -aes256 -out .ssh/id_rsa 2048')
	parser.add_argument('--mkcert', action='store_true', help='generate cert -  openssl req -new -x509 -key .ssh/id_rsa -out .ssh/id_rsa.cert.pem')
	parser.add_argument('--getcert', type=str, help='download the given cert -  curl -R -O url/of/cert.pem')
	parser.add_argument('--browse', dest='browse', type=str, help='open pages with browser')
	args = parser.parse_args(sys.argv[1:])
	if args.mkkey or args.mkcert or args.getcert:
		ssh_dir = os.path.join(home(),'.ssh')
		if not os.path.exists(ssh_dir):
			os.mkdir(ssh_dir)
		key_filename = os.path.join(ssh_dir,'id_rsa')
		got_key = os.path.exists(key_filename)
		if args.mkkey:
			if got_key:
				print 'found existing key - not overwriting - remove manually if overwriting desired: %s' % repr(key_filename)
			else:
				mkkey_cmd = 'openssl genrsa -aes256 -out %s 2048' % key_filename
				print mkkey_cmd
				mkkey_exit = os.system(mkkey_cmd)
				got_key = mkkey_exit == 0
			if not got_key:
				return 1
		cert_filename = os.path.join(ssh_dir,'id_rsa.cert.pem')
		got_cert = os.path.exists(cert_filename)
		if args.mkcert:
			if not got_key:
				return 1
			if got_cert:
				print 'found existing cert - not overwriting - remove manually if overwriting desired: %s' % repr(cert_filename)
			else:
				mkcert_cmd = 'openssl req -new -x509 -key %s -out %s' % (key_filename, cert_filename)
				print mkcert_cmd
				mkcert_exit = os.system(mkcert_cmd)
				got_cert = mkcert_exit == 0
			if not got_cert:
				return 1
		if args.getcert:
			config_home = os.path.join(home(),'.ende-ui')
			if os.path.exists(config_home):
				assert os.path.isdir(config_home), "ERROR: config_home exists but is not a directory: %s" % repr(config_home)
			else:
				os.mkdir(config_home)
			cert_home = os.path.join(config_home,'cert')
			if os.path.exists(cert_home):
				assert os.path.isdir(cert_home), "ERROR: cert_home exists but is not a directory: %s" % repr(cert_home)
			else:
				os.mkdir(cert_home)
			if sys.platform=='win32': # windows - not expecting to find curl
				cert_filename = os.path.basename(args.getcert)
				cert_full_filename = os.path.join(cert_home,cert_filename)
				print 'Getting %s from %s ...' % (repr(cert_full_filename), args.getcert)
				cert_stream = urllib.urlopen(args.getcert)
				cert_body = cert_stream.read()
				open(cert_full_filename,'wb').write(cert_body)
				try:
					cert_ts = email.utils.mktime_tz(email.utils.parsedate_tz(cert_stream.headers['Last-Modified']))
					os.utime(cert_full_filename, (cert_ts, cert_ts))
				except Exception, e:
					cert_ts = None
					print 'WARNING: failed to parse / set timestamp: %s' % e
				print 'Getting %s completed.' % repr(cert_full_filename)
			else: # posix
				curl_args = ['curl', '-R', '-O', args.getcert]
				if args.debug:
					print 'curl_args:', curl_args
					print 'cert_home:', repr(cert_home)
				curl_exit = subprocess.call(curl_args, cwd=cert_home)
				if not curl_exit:
					return 1
		return 0
	print '# Links:'
	print
	print '# SMIME:'
	print '# http://127.0.0.1:%s/ensmime' % args.port
	print '# http://127.0.0.1:%s/desmime' % args.port
	print
	print '# GPG:'
	print '# http://127.0.0.1:%s/engpg' % args.port
	print '# http://127.0.0.1:%s/degpg' % args.port
	print
	print '# Shared Keys ( AES, DES ):'
	print '# http://127.0.0.1:%s/encrypt' % args.port
	print '# http://127.0.0.1:%s/decrypt' % args.port
	print
	if args.browse:
		if sys.platform == 'win32':
			browse_cmd = 'start http://127.0.0.1:%s/%s' % (args.port, args.browse)
		elif sys.platform == 'darwin':
			browse_cmd = 'open http://127.0.0.1:%s/%s' % (args.port, args.browse)
		else:
			print '# browser command not supported for this platform'
			print '# open this url: http://127.0.0.1:%s/%s' % (args.port, args.browse)
			browse_cmd = None
		if browse_cmd:
			os.system(browse_cmd)
	app.run(host=args.host, port=args.port, debug=args.debug)

if __name__ == "__main__":
	sys.exit(main())
