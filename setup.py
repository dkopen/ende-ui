#!/usr/bin/python

from setuptools import setup
from version import VERSION

if __name__ == '__main__':
	setup(
		name="ende-ui",
		version=VERSION,
		packages = ['_ende_ui'],
		package_dir = {'_ende_ui': ''},
		install_requires=[
			'flask',
			'argparse',
		],
		entry_points = {'console_scripts': ['ende-ui = _ende_ui.main:main']},
		data_files = [
			('.',['Makefile']),
		],
		zip_safe=False,
	)
